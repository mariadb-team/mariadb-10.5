/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_ORA_HOME_BUILDBOT_GIT_MKDIST_SQL_YY_ORACLE_HH_INCLUDED
# define YY_ORA_HOME_BUILDBOT_GIT_MKDIST_SQL_YY_ORACLE_HH_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int ORAdebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    ABORT_SYM = 258,               /* ABORT_SYM  */
    IMPOSSIBLE_ACTION = 259,       /* IMPOSSIBLE_ACTION  */
    FORCE_LOOKAHEAD = 260,         /* FORCE_LOOKAHEAD  */
    END_OF_INPUT = 261,            /* END_OF_INPUT  */
    COLON_ORACLE_SYM = 262,        /* COLON_ORACLE_SYM  */
    PARAM_MARKER = 263,            /* PARAM_MARKER  */
    FOR_SYSTEM_TIME_SYM = 264,     /* FOR_SYSTEM_TIME_SYM  */
    LEFT_PAREN_ALT = 265,          /* LEFT_PAREN_ALT  */
    LEFT_PAREN_WITH = 266,         /* LEFT_PAREN_WITH  */
    LEFT_PAREN_LIKE = 267,         /* LEFT_PAREN_LIKE  */
    ORACLE_CONCAT_SYM = 268,       /* ORACLE_CONCAT_SYM  */
    PERCENT_ORACLE_SYM = 269,      /* PERCENT_ORACLE_SYM  */
    WITH_CUBE_SYM = 270,           /* WITH_CUBE_SYM  */
    WITH_ROLLUP_SYM = 271,         /* WITH_ROLLUP_SYM  */
    WITH_SYSTEM_SYM = 272,         /* WITH_SYSTEM_SYM  */
    IDENT = 273,                   /* IDENT  */
    IDENT_QUOTED = 274,            /* IDENT_QUOTED  */
    LEX_HOSTNAME = 275,            /* LEX_HOSTNAME  */
    UNDERSCORE_CHARSET = 276,      /* UNDERSCORE_CHARSET  */
    BIN_NUM = 277,                 /* BIN_NUM  */
    DECIMAL_NUM = 278,             /* DECIMAL_NUM  */
    FLOAT_NUM = 279,               /* FLOAT_NUM  */
    HEX_NUM = 280,                 /* HEX_NUM  */
    HEX_STRING = 281,              /* HEX_STRING  */
    LONG_NUM = 282,                /* LONG_NUM  */
    NCHAR_STRING = 283,            /* NCHAR_STRING  */
    NUM = 284,                     /* NUM  */
    TEXT_STRING = 285,             /* TEXT_STRING  */
    ULONGLONG_NUM = 286,           /* ULONGLONG_NUM  */
    AND_AND_SYM = 287,             /* AND_AND_SYM  */
    DOT_DOT_SYM = 288,             /* DOT_DOT_SYM  */
    EQUAL_SYM = 289,               /* EQUAL_SYM  */
    GE = 290,                      /* GE  */
    LE = 291,                      /* LE  */
    MYSQL_CONCAT_SYM = 292,        /* MYSQL_CONCAT_SYM  */
    NE = 293,                      /* NE  */
    NOT2_SYM = 294,                /* NOT2_SYM  */
    OR2_SYM = 295,                 /* OR2_SYM  */
    SET_VAR = 296,                 /* SET_VAR  */
    SHIFT_LEFT = 297,              /* SHIFT_LEFT  */
    SHIFT_RIGHT = 298,             /* SHIFT_RIGHT  */
    ACCESSIBLE_SYM = 299,          /* ACCESSIBLE_SYM  */
    ADD = 300,                     /* ADD  */
    ALL = 301,                     /* ALL  */
    ALTER = 302,                   /* ALTER  */
    ANALYZE_SYM = 303,             /* ANALYZE_SYM  */
    AND_SYM = 304,                 /* AND_SYM  */
    ASC = 305,                     /* ASC  */
    ASENSITIVE_SYM = 306,          /* ASENSITIVE_SYM  */
    AS = 307,                      /* AS  */
    BEFORE_SYM = 308,              /* BEFORE_SYM  */
    BETWEEN_SYM = 309,             /* BETWEEN_SYM  */
    BIGINT = 310,                  /* BIGINT  */
    BINARY = 311,                  /* BINARY  */
    BIT_AND = 312,                 /* BIT_AND  */
    BIT_OR = 313,                  /* BIT_OR  */
    BIT_XOR = 314,                 /* BIT_XOR  */
    BLOB_MARIADB_SYM = 315,        /* BLOB_MARIADB_SYM  */
    BLOB_ORACLE_SYM = 316,         /* BLOB_ORACLE_SYM  */
    BODY_ORACLE_SYM = 317,         /* BODY_ORACLE_SYM  */
    BOTH = 318,                    /* BOTH  */
    BY = 319,                      /* BY  */
    CALL_SYM = 320,                /* CALL_SYM  */
    CASCADE = 321,                 /* CASCADE  */
    CASE_SYM = 322,                /* CASE_SYM  */
    CAST_SYM = 323,                /* CAST_SYM  */
    CHANGE = 324,                  /* CHANGE  */
    CHAR_SYM = 325,                /* CHAR_SYM  */
    CHECK_SYM = 326,               /* CHECK_SYM  */
    COLLATE_SYM = 327,             /* COLLATE_SYM  */
    CONDITION_SYM = 328,           /* CONDITION_SYM  */
    CONSTRAINT = 329,              /* CONSTRAINT  */
    CONTINUE_MARIADB_SYM = 330,    /* CONTINUE_MARIADB_SYM  */
    CONTINUE_ORACLE_SYM = 331,     /* CONTINUE_ORACLE_SYM  */
    CONVERT_SYM = 332,             /* CONVERT_SYM  */
    COUNT_SYM = 333,               /* COUNT_SYM  */
    CREATE = 334,                  /* CREATE  */
    CROSS = 335,                   /* CROSS  */
    CUME_DIST_SYM = 336,           /* CUME_DIST_SYM  */
    CURDATE = 337,                 /* CURDATE  */
    CURRENT_ROLE = 338,            /* CURRENT_ROLE  */
    CURRENT_USER = 339,            /* CURRENT_USER  */
    CURSOR_SYM = 340,              /* CURSOR_SYM  */
    CURTIME = 341,                 /* CURTIME  */
    DATABASE = 342,                /* DATABASE  */
    DATABASES = 343,               /* DATABASES  */
    DATE_ADD_INTERVAL = 344,       /* DATE_ADD_INTERVAL  */
    DATE_SUB_INTERVAL = 345,       /* DATE_SUB_INTERVAL  */
    DAY_HOUR_SYM = 346,            /* DAY_HOUR_SYM  */
    DAY_MICROSECOND_SYM = 347,     /* DAY_MICROSECOND_SYM  */
    DAY_MINUTE_SYM = 348,          /* DAY_MINUTE_SYM  */
    DAY_SECOND_SYM = 349,          /* DAY_SECOND_SYM  */
    DECIMAL_SYM = 350,             /* DECIMAL_SYM  */
    DECLARE_MARIADB_SYM = 351,     /* DECLARE_MARIADB_SYM  */
    DECLARE_ORACLE_SYM = 352,      /* DECLARE_ORACLE_SYM  */
    DEFAULT = 353,                 /* DEFAULT  */
    DELETE_DOMAIN_ID_SYM = 354,    /* DELETE_DOMAIN_ID_SYM  */
    DELETE_SYM = 355,              /* DELETE_SYM  */
    DENSE_RANK_SYM = 356,          /* DENSE_RANK_SYM  */
    DESCRIBE = 357,                /* DESCRIBE  */
    DESC = 358,                    /* DESC  */
    DETERMINISTIC_SYM = 359,       /* DETERMINISTIC_SYM  */
    DISTINCT = 360,                /* DISTINCT  */
    DIV_SYM = 361,                 /* DIV_SYM  */
    DO_DOMAIN_IDS_SYM = 362,       /* DO_DOMAIN_IDS_SYM  */
    DOUBLE_SYM = 363,              /* DOUBLE_SYM  */
    DROP = 364,                    /* DROP  */
    DUAL_SYM = 365,                /* DUAL_SYM  */
    EACH_SYM = 366,                /* EACH_SYM  */
    ELSEIF_MARIADB_SYM = 367,      /* ELSEIF_MARIADB_SYM  */
    ELSE = 368,                    /* ELSE  */
    ELSIF_ORACLE_SYM = 369,        /* ELSIF_ORACLE_SYM  */
    ENCLOSED = 370,                /* ENCLOSED  */
    ESCAPED = 371,                 /* ESCAPED  */
    EXCEPT_SYM = 372,              /* EXCEPT_SYM  */
    EXISTS = 373,                  /* EXISTS  */
    EXTRACT_SYM = 374,             /* EXTRACT_SYM  */
    FALSE_SYM = 375,               /* FALSE_SYM  */
    FETCH_SYM = 376,               /* FETCH_SYM  */
    FIRST_VALUE_SYM = 377,         /* FIRST_VALUE_SYM  */
    FLOAT_SYM = 378,               /* FLOAT_SYM  */
    FOREIGN = 379,                 /* FOREIGN  */
    FOR_SYM = 380,                 /* FOR_SYM  */
    FROM = 381,                    /* FROM  */
    FULLTEXT_SYM = 382,            /* FULLTEXT_SYM  */
    GOTO_ORACLE_SYM = 383,         /* GOTO_ORACLE_SYM  */
    GRANT = 384,                   /* GRANT  */
    GROUP_CONCAT_SYM = 385,        /* GROUP_CONCAT_SYM  */
    JSON_ARRAYAGG_SYM = 386,       /* JSON_ARRAYAGG_SYM  */
    JSON_OBJECTAGG_SYM = 387,      /* JSON_OBJECTAGG_SYM  */
    GROUP_SYM = 388,               /* GROUP_SYM  */
    HAVING = 389,                  /* HAVING  */
    HOUR_MICROSECOND_SYM = 390,    /* HOUR_MICROSECOND_SYM  */
    HOUR_MINUTE_SYM = 391,         /* HOUR_MINUTE_SYM  */
    HOUR_SECOND_SYM = 392,         /* HOUR_SECOND_SYM  */
    IF_SYM = 393,                  /* IF_SYM  */
    IGNORE_DOMAIN_IDS_SYM = 394,   /* IGNORE_DOMAIN_IDS_SYM  */
    IGNORE_SYM = 395,              /* IGNORE_SYM  */
    INDEX_SYM = 396,               /* INDEX_SYM  */
    INFILE = 397,                  /* INFILE  */
    INNER_SYM = 398,               /* INNER_SYM  */
    INOUT_SYM = 399,               /* INOUT_SYM  */
    INSENSITIVE_SYM = 400,         /* INSENSITIVE_SYM  */
    INSERT = 401,                  /* INSERT  */
    IN_SYM = 402,                  /* IN_SYM  */
    INTERSECT_SYM = 403,           /* INTERSECT_SYM  */
    INTERVAL_SYM = 404,            /* INTERVAL_SYM  */
    INTO = 405,                    /* INTO  */
    INT_SYM = 406,                 /* INT_SYM  */
    IS = 407,                      /* IS  */
    ITERATE_SYM = 408,             /* ITERATE_SYM  */
    JOIN_SYM = 409,                /* JOIN_SYM  */
    KEYS = 410,                    /* KEYS  */
    KEY_SYM = 411,                 /* KEY_SYM  */
    KILL_SYM = 412,                /* KILL_SYM  */
    LAG_SYM = 413,                 /* LAG_SYM  */
    LEADING = 414,                 /* LEADING  */
    LEAD_SYM = 415,                /* LEAD_SYM  */
    LEAVE_SYM = 416,               /* LEAVE_SYM  */
    LEFT = 417,                    /* LEFT  */
    LIKE = 418,                    /* LIKE  */
    LIMIT = 419,                   /* LIMIT  */
    LINEAR_SYM = 420,              /* LINEAR_SYM  */
    LINES = 421,                   /* LINES  */
    LOAD = 422,                    /* LOAD  */
    LOCATOR_SYM = 423,             /* LOCATOR_SYM  */
    LOCK_SYM = 424,                /* LOCK_SYM  */
    LONGBLOB = 425,                /* LONGBLOB  */
    LONG_SYM = 426,                /* LONG_SYM  */
    LONGTEXT = 427,                /* LONGTEXT  */
    LOOP_SYM = 428,                /* LOOP_SYM  */
    LOW_PRIORITY = 429,            /* LOW_PRIORITY  */
    MASTER_SSL_VERIFY_SERVER_CERT_SYM = 430, /* MASTER_SSL_VERIFY_SERVER_CERT_SYM  */
    MATCH = 431,                   /* MATCH  */
    MAX_SYM = 432,                 /* MAX_SYM  */
    MAXVALUE_SYM = 433,            /* MAXVALUE_SYM  */
    MEDIAN_SYM = 434,              /* MEDIAN_SYM  */
    MEDIUMBLOB = 435,              /* MEDIUMBLOB  */
    MEDIUMINT = 436,               /* MEDIUMINT  */
    MEDIUMTEXT = 437,              /* MEDIUMTEXT  */
    MIN_SYM = 438,                 /* MIN_SYM  */
    MINUTE_MICROSECOND_SYM = 439,  /* MINUTE_MICROSECOND_SYM  */
    MINUTE_SECOND_SYM = 440,       /* MINUTE_SECOND_SYM  */
    MODIFIES_SYM = 441,            /* MODIFIES_SYM  */
    MOD_SYM = 442,                 /* MOD_SYM  */
    NATURAL = 443,                 /* NATURAL  */
    NEG = 444,                     /* NEG  */
    NOT_SYM = 445,                 /* NOT_SYM  */
    NO_WRITE_TO_BINLOG = 446,      /* NO_WRITE_TO_BINLOG  */
    NOW_SYM = 447,                 /* NOW_SYM  */
    NTH_VALUE_SYM = 448,           /* NTH_VALUE_SYM  */
    NTILE_SYM = 449,               /* NTILE_SYM  */
    NULL_SYM = 450,                /* NULL_SYM  */
    NUMERIC_SYM = 451,             /* NUMERIC_SYM  */
    ON = 452,                      /* ON  */
    OPTIMIZE = 453,                /* OPTIMIZE  */
    OPTIONALLY = 454,              /* OPTIONALLY  */
    ORDER_SYM = 455,               /* ORDER_SYM  */
    OR_SYM = 456,                  /* OR_SYM  */
    OTHERS_ORACLE_SYM = 457,       /* OTHERS_ORACLE_SYM  */
    OUTER = 458,                   /* OUTER  */
    OUTFILE = 459,                 /* OUTFILE  */
    OUT_SYM = 460,                 /* OUT_SYM  */
    OVER_SYM = 461,                /* OVER_SYM  */
    PACKAGE_ORACLE_SYM = 462,      /* PACKAGE_ORACLE_SYM  */
    PAGE_CHECKSUM_SYM = 463,       /* PAGE_CHECKSUM_SYM  */
    PARSE_VCOL_EXPR_SYM = 464,     /* PARSE_VCOL_EXPR_SYM  */
    PARTITION_SYM = 465,           /* PARTITION_SYM  */
    PERCENTILE_CONT_SYM = 466,     /* PERCENTILE_CONT_SYM  */
    PERCENTILE_DISC_SYM = 467,     /* PERCENTILE_DISC_SYM  */
    PERCENT_RANK_SYM = 468,        /* PERCENT_RANK_SYM  */
    PORTION_SYM = 469,             /* PORTION_SYM  */
    POSITION_SYM = 470,            /* POSITION_SYM  */
    PRECISION = 471,               /* PRECISION  */
    PRIMARY_SYM = 472,             /* PRIMARY_SYM  */
    PROCEDURE_SYM = 473,           /* PROCEDURE_SYM  */
    PURGE = 474,                   /* PURGE  */
    RAISE_ORACLE_SYM = 475,        /* RAISE_ORACLE_SYM  */
    RANGE_SYM = 476,               /* RANGE_SYM  */
    RANK_SYM = 477,                /* RANK_SYM  */
    READS_SYM = 478,               /* READS_SYM  */
    READ_SYM = 479,                /* READ_SYM  */
    READ_WRITE_SYM = 480,          /* READ_WRITE_SYM  */
    REAL = 481,                    /* REAL  */
    RECURSIVE_SYM = 482,           /* RECURSIVE_SYM  */
    REFERENCES = 483,              /* REFERENCES  */
    REF_SYSTEM_ID_SYM = 484,       /* REF_SYSTEM_ID_SYM  */
    REGEXP = 485,                  /* REGEXP  */
    RELEASE_SYM = 486,             /* RELEASE_SYM  */
    RENAME = 487,                  /* RENAME  */
    REPEAT_SYM = 488,              /* REPEAT_SYM  */
    REQUIRE_SYM = 489,             /* REQUIRE_SYM  */
    RESIGNAL_SYM = 490,            /* RESIGNAL_SYM  */
    RESTRICT = 491,                /* RESTRICT  */
    RETURNING_SYM = 492,           /* RETURNING_SYM  */
    RETURN_MARIADB_SYM = 493,      /* RETURN_MARIADB_SYM  */
    RETURN_ORACLE_SYM = 494,       /* RETURN_ORACLE_SYM  */
    REVOKE = 495,                  /* REVOKE  */
    RIGHT = 496,                   /* RIGHT  */
    ROW_NUMBER_SYM = 497,          /* ROW_NUMBER_SYM  */
    ROWS_SYM = 498,                /* ROWS_SYM  */
    ROWTYPE_ORACLE_SYM = 499,      /* ROWTYPE_ORACLE_SYM  */
    SECOND_MICROSECOND_SYM = 500,  /* SECOND_MICROSECOND_SYM  */
    SELECT_SYM = 501,              /* SELECT_SYM  */
    SENSITIVE_SYM = 502,           /* SENSITIVE_SYM  */
    SEPARATOR_SYM = 503,           /* SEPARATOR_SYM  */
    SERVER_OPTIONS = 504,          /* SERVER_OPTIONS  */
    SET = 505,                     /* SET  */
    SHOW = 506,                    /* SHOW  */
    SIGNAL_SYM = 507,              /* SIGNAL_SYM  */
    SMALLINT = 508,                /* SMALLINT  */
    SPATIAL_SYM = 509,             /* SPATIAL_SYM  */
    SPECIFIC_SYM = 510,            /* SPECIFIC_SYM  */
    SQL_BIG_RESULT = 511,          /* SQL_BIG_RESULT  */
    SQLEXCEPTION_SYM = 512,        /* SQLEXCEPTION_SYM  */
    SQL_SMALL_RESULT = 513,        /* SQL_SMALL_RESULT  */
    SQLSTATE_SYM = 514,            /* SQLSTATE_SYM  */
    SQL_SYM = 515,                 /* SQL_SYM  */
    SQLWARNING_SYM = 516,          /* SQLWARNING_SYM  */
    SSL_SYM = 517,                 /* SSL_SYM  */
    STARTING = 518,                /* STARTING  */
    STATS_AUTO_RECALC_SYM = 519,   /* STATS_AUTO_RECALC_SYM  */
    STATS_PERSISTENT_SYM = 520,    /* STATS_PERSISTENT_SYM  */
    STATS_SAMPLE_PAGES_SYM = 521,  /* STATS_SAMPLE_PAGES_SYM  */
    STDDEV_SAMP_SYM = 522,         /* STDDEV_SAMP_SYM  */
    STD_SYM = 523,                 /* STD_SYM  */
    STRAIGHT_JOIN = 524,           /* STRAIGHT_JOIN  */
    SUM_SYM = 525,                 /* SUM_SYM  */
    SYSDATE = 526,                 /* SYSDATE  */
    TABLE_REF_PRIORITY = 527,      /* TABLE_REF_PRIORITY  */
    TABLE_SYM = 528,               /* TABLE_SYM  */
    TERMINATED = 529,              /* TERMINATED  */
    THEN_SYM = 530,                /* THEN_SYM  */
    TINYBLOB = 531,                /* TINYBLOB  */
    TINYINT = 532,                 /* TINYINT  */
    TINYTEXT = 533,                /* TINYTEXT  */
    TO_SYM = 534,                  /* TO_SYM  */
    TRAILING = 535,                /* TRAILING  */
    TRIGGER_SYM = 536,             /* TRIGGER_SYM  */
    TRUE_SYM = 537,                /* TRUE_SYM  */
    UNDO_SYM = 538,                /* UNDO_SYM  */
    UNION_SYM = 539,               /* UNION_SYM  */
    UNIQUE_SYM = 540,              /* UNIQUE_SYM  */
    UNLOCK_SYM = 541,              /* UNLOCK_SYM  */
    UNSIGNED = 542,                /* UNSIGNED  */
    UPDATE_SYM = 543,              /* UPDATE_SYM  */
    USAGE = 544,                   /* USAGE  */
    USE_SYM = 545,                 /* USE_SYM  */
    USING = 546,                   /* USING  */
    UTC_DATE_SYM = 547,            /* UTC_DATE_SYM  */
    UTC_TIMESTAMP_SYM = 548,       /* UTC_TIMESTAMP_SYM  */
    UTC_TIME_SYM = 549,            /* UTC_TIME_SYM  */
    VALUES_IN_SYM = 550,           /* VALUES_IN_SYM  */
    VALUES_LESS_SYM = 551,         /* VALUES_LESS_SYM  */
    VALUES = 552,                  /* VALUES  */
    VARBINARY = 553,               /* VARBINARY  */
    VARCHAR = 554,                 /* VARCHAR  */
    VARIANCE_SYM = 555,            /* VARIANCE_SYM  */
    VAR_SAMP_SYM = 556,            /* VAR_SAMP_SYM  */
    VARYING = 557,                 /* VARYING  */
    WHEN_SYM = 558,                /* WHEN_SYM  */
    WHERE = 559,                   /* WHERE  */
    WHILE_SYM = 560,               /* WHILE_SYM  */
    WITH = 561,                    /* WITH  */
    XOR = 562,                     /* XOR  */
    YEAR_MONTH_SYM = 563,          /* YEAR_MONTH_SYM  */
    ZEROFILL = 564,                /* ZEROFILL  */
    BODY_MARIADB_SYM = 565,        /* BODY_MARIADB_SYM  */
    ELSEIF_ORACLE_SYM = 566,       /* ELSEIF_ORACLE_SYM  */
    ELSIF_MARIADB_SYM = 567,       /* ELSIF_MARIADB_SYM  */
    EXCEPTION_ORACLE_SYM = 568,    /* EXCEPTION_ORACLE_SYM  */
    GOTO_MARIADB_SYM = 569,        /* GOTO_MARIADB_SYM  */
    OTHERS_MARIADB_SYM = 570,      /* OTHERS_MARIADB_SYM  */
    PACKAGE_MARIADB_SYM = 571,     /* PACKAGE_MARIADB_SYM  */
    RAISE_MARIADB_SYM = 572,       /* RAISE_MARIADB_SYM  */
    ROWTYPE_MARIADB_SYM = 573,     /* ROWTYPE_MARIADB_SYM  */
    REPLACE = 574,                 /* REPLACE  */
    SUBSTRING = 575,               /* SUBSTRING  */
    TRIM = 576,                    /* TRIM  */
    ACCOUNT_SYM = 577,             /* ACCOUNT_SYM  */
    ACTION = 578,                  /* ACTION  */
    ADMIN_SYM = 579,               /* ADMIN_SYM  */
    ADDDATE_SYM = 580,             /* ADDDATE_SYM  */
    AFTER_SYM = 581,               /* AFTER_SYM  */
    AGAINST = 582,                 /* AGAINST  */
    AGGREGATE_SYM = 583,           /* AGGREGATE_SYM  */
    ALGORITHM_SYM = 584,           /* ALGORITHM_SYM  */
    ALWAYS_SYM = 585,              /* ALWAYS_SYM  */
    ANY_SYM = 586,                 /* ANY_SYM  */
    ASCII_SYM = 587,               /* ASCII_SYM  */
    AT_SYM = 588,                  /* AT_SYM  */
    ATOMIC_SYM = 589,              /* ATOMIC_SYM  */
    AUTHORS_SYM = 590,             /* AUTHORS_SYM  */
    AUTOEXTEND_SIZE_SYM = 591,     /* AUTOEXTEND_SIZE_SYM  */
    AUTO_INC = 592,                /* AUTO_INC  */
    AUTO_SYM = 593,                /* AUTO_SYM  */
    AVG_ROW_LENGTH = 594,          /* AVG_ROW_LENGTH  */
    AVG_SYM = 595,                 /* AVG_SYM  */
    BACKUP_SYM = 596,              /* BACKUP_SYM  */
    BEGIN_MARIADB_SYM = 597,       /* BEGIN_MARIADB_SYM  */
    BEGIN_ORACLE_SYM = 598,        /* BEGIN_ORACLE_SYM  */
    BINLOG_SYM = 599,              /* BINLOG_SYM  */
    BIT_SYM = 600,                 /* BIT_SYM  */
    BLOCK_SYM = 601,               /* BLOCK_SYM  */
    BOOL_SYM = 602,                /* BOOL_SYM  */
    BOOLEAN_SYM = 603,             /* BOOLEAN_SYM  */
    BTREE_SYM = 604,               /* BTREE_SYM  */
    BYTE_SYM = 605,                /* BYTE_SYM  */
    CACHE_SYM = 606,               /* CACHE_SYM  */
    CASCADED = 607,                /* CASCADED  */
    CATALOG_NAME_SYM = 608,        /* CATALOG_NAME_SYM  */
    CHAIN_SYM = 609,               /* CHAIN_SYM  */
    CHANGED = 610,                 /* CHANGED  */
    CHARSET = 611,                 /* CHARSET  */
    CHECKPOINT_SYM = 612,          /* CHECKPOINT_SYM  */
    CHECKSUM_SYM = 613,            /* CHECKSUM_SYM  */
    CIPHER_SYM = 614,              /* CIPHER_SYM  */
    CLASS_ORIGIN_SYM = 615,        /* CLASS_ORIGIN_SYM  */
    CLIENT_SYM = 616,              /* CLIENT_SYM  */
    CLOB_MARIADB_SYM = 617,        /* CLOB_MARIADB_SYM  */
    CLOB_ORACLE_SYM = 618,         /* CLOB_ORACLE_SYM  */
    CLOSE_SYM = 619,               /* CLOSE_SYM  */
    COALESCE = 620,                /* COALESCE  */
    CODE_SYM = 621,                /* CODE_SYM  */
    COLLATION_SYM = 622,           /* COLLATION_SYM  */
    COLUMNS = 623,                 /* COLUMNS  */
    COLUMN_ADD_SYM = 624,          /* COLUMN_ADD_SYM  */
    COLUMN_CHECK_SYM = 625,        /* COLUMN_CHECK_SYM  */
    COLUMN_CREATE_SYM = 626,       /* COLUMN_CREATE_SYM  */
    COLUMN_DELETE_SYM = 627,       /* COLUMN_DELETE_SYM  */
    COLUMN_GET_SYM = 628,          /* COLUMN_GET_SYM  */
    COLUMN_SYM = 629,              /* COLUMN_SYM  */
    COLUMN_NAME_SYM = 630,         /* COLUMN_NAME_SYM  */
    COMMENT_SYM = 631,             /* COMMENT_SYM  */
    COMMITTED_SYM = 632,           /* COMMITTED_SYM  */
    COMMIT_SYM = 633,              /* COMMIT_SYM  */
    COMPACT_SYM = 634,             /* COMPACT_SYM  */
    COMPLETION_SYM = 635,          /* COMPLETION_SYM  */
    COMPRESSED_SYM = 636,          /* COMPRESSED_SYM  */
    CONCURRENT = 637,              /* CONCURRENT  */
    CONNECTION_SYM = 638,          /* CONNECTION_SYM  */
    CONSISTENT_SYM = 639,          /* CONSISTENT_SYM  */
    CONSTRAINT_CATALOG_SYM = 640,  /* CONSTRAINT_CATALOG_SYM  */
    CONSTRAINT_NAME_SYM = 641,     /* CONSTRAINT_NAME_SYM  */
    CONSTRAINT_SCHEMA_SYM = 642,   /* CONSTRAINT_SCHEMA_SYM  */
    CONTAINS_SYM = 643,            /* CONTAINS_SYM  */
    CONTEXT_SYM = 644,             /* CONTEXT_SYM  */
    CONTRIBUTORS_SYM = 645,        /* CONTRIBUTORS_SYM  */
    CPU_SYM = 646,                 /* CPU_SYM  */
    CUBE_SYM = 647,                /* CUBE_SYM  */
    CURRENT_SYM = 648,             /* CURRENT_SYM  */
    CURRENT_POS_SYM = 649,         /* CURRENT_POS_SYM  */
    CURSOR_NAME_SYM = 650,         /* CURSOR_NAME_SYM  */
    CYCLE_SYM = 651,               /* CYCLE_SYM  */
    DATAFILE_SYM = 652,            /* DATAFILE_SYM  */
    DATA_SYM = 653,                /* DATA_SYM  */
    DATETIME = 654,                /* DATETIME  */
    DATE_FORMAT_SYM = 655,         /* DATE_FORMAT_SYM  */
    DATE_SYM = 656,                /* DATE_SYM  */
    DAY_SYM = 657,                 /* DAY_SYM  */
    DEALLOCATE_SYM = 658,          /* DEALLOCATE_SYM  */
    DEFINER_SYM = 659,             /* DEFINER_SYM  */
    DELAYED_SYM = 660,             /* DELAYED_SYM  */
    DELAY_KEY_WRITE_SYM = 661,     /* DELAY_KEY_WRITE_SYM  */
    DES_KEY_FILE = 662,            /* DES_KEY_FILE  */
    DIAGNOSTICS_SYM = 663,         /* DIAGNOSTICS_SYM  */
    DIRECTORY_SYM = 664,           /* DIRECTORY_SYM  */
    DISABLE_SYM = 665,             /* DISABLE_SYM  */
    DISCARD = 666,                 /* DISCARD  */
    DISK_SYM = 667,                /* DISK_SYM  */
    DO_SYM = 668,                  /* DO_SYM  */
    DUMPFILE = 669,                /* DUMPFILE  */
    DUPLICATE_SYM = 670,           /* DUPLICATE_SYM  */
    DYNAMIC_SYM = 671,             /* DYNAMIC_SYM  */
    ENABLE_SYM = 672,              /* ENABLE_SYM  */
    END = 673,                     /* END  */
    ENDS_SYM = 674,                /* ENDS_SYM  */
    ENGINES_SYM = 675,             /* ENGINES_SYM  */
    ENGINE_SYM = 676,              /* ENGINE_SYM  */
    ENUM = 677,                    /* ENUM  */
    ERROR_SYM = 678,               /* ERROR_SYM  */
    ERRORS = 679,                  /* ERRORS  */
    ESCAPE_SYM = 680,              /* ESCAPE_SYM  */
    EVENTS_SYM = 681,              /* EVENTS_SYM  */
    EVENT_SYM = 682,               /* EVENT_SYM  */
    EVERY_SYM = 683,               /* EVERY_SYM  */
    EXCHANGE_SYM = 684,            /* EXCHANGE_SYM  */
    EXAMINED_SYM = 685,            /* EXAMINED_SYM  */
    EXCLUDE_SYM = 686,             /* EXCLUDE_SYM  */
    EXECUTE_SYM = 687,             /* EXECUTE_SYM  */
    EXCEPTION_MARIADB_SYM = 688,   /* EXCEPTION_MARIADB_SYM  */
    EXIT_MARIADB_SYM = 689,        /* EXIT_MARIADB_SYM  */
    EXIT_ORACLE_SYM = 690,         /* EXIT_ORACLE_SYM  */
    EXPANSION_SYM = 691,           /* EXPANSION_SYM  */
    EXPIRE_SYM = 692,              /* EXPIRE_SYM  */
    EXPORT_SYM = 693,              /* EXPORT_SYM  */
    EXTENDED_SYM = 694,            /* EXTENDED_SYM  */
    EXTENT_SIZE_SYM = 695,         /* EXTENT_SIZE_SYM  */
    FAST_SYM = 696,                /* FAST_SYM  */
    FAULTS_SYM = 697,              /* FAULTS_SYM  */
    FEDERATED_SYM = 698,           /* FEDERATED_SYM  */
    FILE_SYM = 699,                /* FILE_SYM  */
    FIRST_SYM = 700,               /* FIRST_SYM  */
    FIXED_SYM = 701,               /* FIXED_SYM  */
    FLUSH_SYM = 702,               /* FLUSH_SYM  */
    FOLLOWS_SYM = 703,             /* FOLLOWS_SYM  */
    FOLLOWING_SYM = 704,           /* FOLLOWING_SYM  */
    FORCE_SYM = 705,               /* FORCE_SYM  */
    FORMAT_SYM = 706,              /* FORMAT_SYM  */
    FOUND_SYM = 707,               /* FOUND_SYM  */
    FULL = 708,                    /* FULL  */
    FUNCTION_SYM = 709,            /* FUNCTION_SYM  */
    GENERAL = 710,                 /* GENERAL  */
    GENERATED_SYM = 711,           /* GENERATED_SYM  */
    GET_FORMAT = 712,              /* GET_FORMAT  */
    GET_SYM = 713,                 /* GET_SYM  */
    GLOBAL_SYM = 714,              /* GLOBAL_SYM  */
    GRANTS = 715,                  /* GRANTS  */
    HANDLER_SYM = 716,             /* HANDLER_SYM  */
    HARD_SYM = 717,                /* HARD_SYM  */
    HASH_SYM = 718,                /* HASH_SYM  */
    HELP_SYM = 719,                /* HELP_SYM  */
    HIGH_PRIORITY = 720,           /* HIGH_PRIORITY  */
    HISTORY_SYM = 721,             /* HISTORY_SYM  */
    HOST_SYM = 722,                /* HOST_SYM  */
    HOSTS_SYM = 723,               /* HOSTS_SYM  */
    HOUR_SYM = 724,                /* HOUR_SYM  */
    ID_SYM = 725,                  /* ID_SYM  */
    IDENTIFIED_SYM = 726,          /* IDENTIFIED_SYM  */
    IGNORE_SERVER_IDS_SYM = 727,   /* IGNORE_SERVER_IDS_SYM  */
    IMMEDIATE_SYM = 728,           /* IMMEDIATE_SYM  */
    IMPORT = 729,                  /* IMPORT  */
    INCREMENT_SYM = 730,           /* INCREMENT_SYM  */
    INDEXES = 731,                 /* INDEXES  */
    INITIAL_SIZE_SYM = 732,        /* INITIAL_SIZE_SYM  */
    INSERT_METHOD = 733,           /* INSERT_METHOD  */
    INSTALL_SYM = 734,             /* INSTALL_SYM  */
    INVOKER_SYM = 735,             /* INVOKER_SYM  */
    IO_SYM = 736,                  /* IO_SYM  */
    IPC_SYM = 737,                 /* IPC_SYM  */
    ISOLATION = 738,               /* ISOLATION  */
    ISOPEN_SYM = 739,              /* ISOPEN_SYM  */
    ISSUER_SYM = 740,              /* ISSUER_SYM  */
    INVISIBLE_SYM = 741,           /* INVISIBLE_SYM  */
    JSON_SYM = 742,                /* JSON_SYM  */
    KEY_BLOCK_SIZE = 743,          /* KEY_BLOCK_SIZE  */
    LANGUAGE_SYM = 744,            /* LANGUAGE_SYM  */
    LAST_SYM = 745,                /* LAST_SYM  */
    LAST_VALUE = 746,              /* LAST_VALUE  */
    LASTVAL_SYM = 747,             /* LASTVAL_SYM  */
    LEAVES = 748,                  /* LEAVES  */
    LESS_SYM = 749,                /* LESS_SYM  */
    LEVEL_SYM = 750,               /* LEVEL_SYM  */
    LIST_SYM = 751,                /* LIST_SYM  */
    LOCAL_SYM = 752,               /* LOCAL_SYM  */
    LOCKS_SYM = 753,               /* LOCKS_SYM  */
    LOGFILE_SYM = 754,             /* LOGFILE_SYM  */
    LOGS_SYM = 755,                /* LOGS_SYM  */
    MASTER_CONNECT_RETRY_SYM = 756, /* MASTER_CONNECT_RETRY_SYM  */
    MASTER_DELAY_SYM = 757,        /* MASTER_DELAY_SYM  */
    MASTER_GTID_POS_SYM = 758,     /* MASTER_GTID_POS_SYM  */
    MASTER_HOST_SYM = 759,         /* MASTER_HOST_SYM  */
    MASTER_LOG_FILE_SYM = 760,     /* MASTER_LOG_FILE_SYM  */
    MASTER_LOG_POS_SYM = 761,      /* MASTER_LOG_POS_SYM  */
    MASTER_PASSWORD_SYM = 762,     /* MASTER_PASSWORD_SYM  */
    MASTER_PORT_SYM = 763,         /* MASTER_PORT_SYM  */
    MASTER_SERVER_ID_SYM = 764,    /* MASTER_SERVER_ID_SYM  */
    MASTER_SSL_CAPATH_SYM = 765,   /* MASTER_SSL_CAPATH_SYM  */
    MASTER_SSL_CA_SYM = 766,       /* MASTER_SSL_CA_SYM  */
    MASTER_SSL_CERT_SYM = 767,     /* MASTER_SSL_CERT_SYM  */
    MASTER_SSL_CIPHER_SYM = 768,   /* MASTER_SSL_CIPHER_SYM  */
    MASTER_SSL_CRL_SYM = 769,      /* MASTER_SSL_CRL_SYM  */
    MASTER_SSL_CRLPATH_SYM = 770,  /* MASTER_SSL_CRLPATH_SYM  */
    MASTER_SSL_KEY_SYM = 771,      /* MASTER_SSL_KEY_SYM  */
    MASTER_SSL_SYM = 772,          /* MASTER_SSL_SYM  */
    MASTER_SYM = 773,              /* MASTER_SYM  */
    MASTER_USER_SYM = 774,         /* MASTER_USER_SYM  */
    MASTER_USE_GTID_SYM = 775,     /* MASTER_USE_GTID_SYM  */
    MASTER_HEARTBEAT_PERIOD_SYM = 776, /* MASTER_HEARTBEAT_PERIOD_SYM  */
    MAX_CONNECTIONS_PER_HOUR = 777, /* MAX_CONNECTIONS_PER_HOUR  */
    MAX_QUERIES_PER_HOUR = 778,    /* MAX_QUERIES_PER_HOUR  */
    MAX_ROWS = 779,                /* MAX_ROWS  */
    MAX_SIZE_SYM = 780,            /* MAX_SIZE_SYM  */
    MAX_UPDATES_PER_HOUR = 781,    /* MAX_UPDATES_PER_HOUR  */
    MAX_STATEMENT_TIME_SYM = 782,  /* MAX_STATEMENT_TIME_SYM  */
    MAX_USER_CONNECTIONS_SYM = 783, /* MAX_USER_CONNECTIONS_SYM  */
    MEDIUM_SYM = 784,              /* MEDIUM_SYM  */
    MEMORY_SYM = 785,              /* MEMORY_SYM  */
    MERGE_SYM = 786,               /* MERGE_SYM  */
    MESSAGE_TEXT_SYM = 787,        /* MESSAGE_TEXT_SYM  */
    MICROSECOND_SYM = 788,         /* MICROSECOND_SYM  */
    MIGRATE_SYM = 789,             /* MIGRATE_SYM  */
    MINUTE_SYM = 790,              /* MINUTE_SYM  */
    MINVALUE_SYM = 791,            /* MINVALUE_SYM  */
    MIN_ROWS = 792,                /* MIN_ROWS  */
    MODE_SYM = 793,                /* MODE_SYM  */
    MODIFY_SYM = 794,              /* MODIFY_SYM  */
    MONITOR_SYM = 795,             /* MONITOR_SYM  */
    MONTH_SYM = 796,               /* MONTH_SYM  */
    MUTEX_SYM = 797,               /* MUTEX_SYM  */
    MYSQL_SYM = 798,               /* MYSQL_SYM  */
    MYSQL_ERRNO_SYM = 799,         /* MYSQL_ERRNO_SYM  */
    NAMES_SYM = 800,               /* NAMES_SYM  */
    NAME_SYM = 801,                /* NAME_SYM  */
    NATIONAL_SYM = 802,            /* NATIONAL_SYM  */
    NCHAR_SYM = 803,               /* NCHAR_SYM  */
    NEVER_SYM = 804,               /* NEVER_SYM  */
    NEW_SYM = 805,                 /* NEW_SYM  */
    NEXT_SYM = 806,                /* NEXT_SYM  */
    NEXTVAL_SYM = 807,             /* NEXTVAL_SYM  */
    NOCACHE_SYM = 808,             /* NOCACHE_SYM  */
    NOCYCLE_SYM = 809,             /* NOCYCLE_SYM  */
    NODEGROUP_SYM = 810,           /* NODEGROUP_SYM  */
    NONE_SYM = 811,                /* NONE_SYM  */
    NOTFOUND_SYM = 812,            /* NOTFOUND_SYM  */
    NO_SYM = 813,                  /* NO_SYM  */
    NOMAXVALUE_SYM = 814,          /* NOMAXVALUE_SYM  */
    NOMINVALUE_SYM = 815,          /* NOMINVALUE_SYM  */
    NO_WAIT_SYM = 816,             /* NO_WAIT_SYM  */
    NOWAIT_SYM = 817,              /* NOWAIT_SYM  */
    NUMBER_MARIADB_SYM = 818,      /* NUMBER_MARIADB_SYM  */
    NUMBER_ORACLE_SYM = 819,       /* NUMBER_ORACLE_SYM  */
    NVARCHAR_SYM = 820,            /* NVARCHAR_SYM  */
    OF_SYM = 821,                  /* OF_SYM  */
    OFFSET_SYM = 822,              /* OFFSET_SYM  */
    OLD_PASSWORD_SYM = 823,        /* OLD_PASSWORD_SYM  */
    ONE_SYM = 824,                 /* ONE_SYM  */
    ONLY_SYM = 825,                /* ONLY_SYM  */
    ONLINE_SYM = 826,              /* ONLINE_SYM  */
    OPEN_SYM = 827,                /* OPEN_SYM  */
    OPTIONS_SYM = 828,             /* OPTIONS_SYM  */
    OPTION = 829,                  /* OPTION  */
    OVERLAPS_SYM = 830,            /* OVERLAPS_SYM  */
    OWNER_SYM = 831,               /* OWNER_SYM  */
    PACK_KEYS_SYM = 832,           /* PACK_KEYS_SYM  */
    PAGE_SYM = 833,                /* PAGE_SYM  */
    PARSER_SYM = 834,              /* PARSER_SYM  */
    PARTIAL = 835,                 /* PARTIAL  */
    PARTITIONS_SYM = 836,          /* PARTITIONS_SYM  */
    PARTITIONING_SYM = 837,        /* PARTITIONING_SYM  */
    PASSWORD_SYM = 838,            /* PASSWORD_SYM  */
    PERIOD_SYM = 839,              /* PERIOD_SYM  */
    PERSISTENT_SYM = 840,          /* PERSISTENT_SYM  */
    PHASE_SYM = 841,               /* PHASE_SYM  */
    PLUGINS_SYM = 842,             /* PLUGINS_SYM  */
    PLUGIN_SYM = 843,              /* PLUGIN_SYM  */
    PORT_SYM = 844,                /* PORT_SYM  */
    PRECEDES_SYM = 845,            /* PRECEDES_SYM  */
    PRECEDING_SYM = 846,           /* PRECEDING_SYM  */
    PREPARE_SYM = 847,             /* PREPARE_SYM  */
    PRESERVE_SYM = 848,            /* PRESERVE_SYM  */
    PREV_SYM = 849,                /* PREV_SYM  */
    PREVIOUS_SYM = 850,            /* PREVIOUS_SYM  */
    PRIVILEGES = 851,              /* PRIVILEGES  */
    PROCESS = 852,                 /* PROCESS  */
    PROCESSLIST_SYM = 853,         /* PROCESSLIST_SYM  */
    PROFILE_SYM = 854,             /* PROFILE_SYM  */
    PROFILES_SYM = 855,            /* PROFILES_SYM  */
    PROXY_SYM = 856,               /* PROXY_SYM  */
    QUARTER_SYM = 857,             /* QUARTER_SYM  */
    QUERY_SYM = 858,               /* QUERY_SYM  */
    QUICK = 859,                   /* QUICK  */
    RAW_MARIADB_SYM = 860,         /* RAW_MARIADB_SYM  */
    RAW_ORACLE_SYM = 861,          /* RAW_ORACLE_SYM  */
    READ_ONLY_SYM = 862,           /* READ_ONLY_SYM  */
    REBUILD_SYM = 863,             /* REBUILD_SYM  */
    RECOVER_SYM = 864,             /* RECOVER_SYM  */
    REDOFILE_SYM = 865,            /* REDOFILE_SYM  */
    REDO_BUFFER_SIZE_SYM = 866,    /* REDO_BUFFER_SIZE_SYM  */
    REDUNDANT_SYM = 867,           /* REDUNDANT_SYM  */
    RELAY = 868,                   /* RELAY  */
    RELAYLOG_SYM = 869,            /* RELAYLOG_SYM  */
    RELAY_LOG_FILE_SYM = 870,      /* RELAY_LOG_FILE_SYM  */
    RELAY_LOG_POS_SYM = 871,       /* RELAY_LOG_POS_SYM  */
    RELAY_THREAD = 872,            /* RELAY_THREAD  */
    RELOAD = 873,                  /* RELOAD  */
    REMOVE_SYM = 874,              /* REMOVE_SYM  */
    REORGANIZE_SYM = 875,          /* REORGANIZE_SYM  */
    REPAIR = 876,                  /* REPAIR  */
    REPEATABLE_SYM = 877,          /* REPEATABLE_SYM  */
    REPLAY_SYM = 878,              /* REPLAY_SYM  */
    REPLICATION = 879,             /* REPLICATION  */
    RESET_SYM = 880,               /* RESET_SYM  */
    RESTART_SYM = 881,             /* RESTART_SYM  */
    RESOURCES = 882,               /* RESOURCES  */
    RESTORE_SYM = 883,             /* RESTORE_SYM  */
    RESUME_SYM = 884,              /* RESUME_SYM  */
    RETURNED_SQLSTATE_SYM = 885,   /* RETURNED_SQLSTATE_SYM  */
    RETURNS_SYM = 886,             /* RETURNS_SYM  */
    REUSE_SYM = 887,               /* REUSE_SYM  */
    REVERSE_SYM = 888,             /* REVERSE_SYM  */
    ROLE_SYM = 889,                /* ROLE_SYM  */
    ROLLBACK_SYM = 890,            /* ROLLBACK_SYM  */
    ROLLUP_SYM = 891,              /* ROLLUP_SYM  */
    ROUTINE_SYM = 892,             /* ROUTINE_SYM  */
    ROWCOUNT_SYM = 893,            /* ROWCOUNT_SYM  */
    ROW_SYM = 894,                 /* ROW_SYM  */
    ROW_COUNT_SYM = 895,           /* ROW_COUNT_SYM  */
    ROW_FORMAT_SYM = 896,          /* ROW_FORMAT_SYM  */
    RTREE_SYM = 897,               /* RTREE_SYM  */
    SAVEPOINT_SYM = 898,           /* SAVEPOINT_SYM  */
    SCHEDULE_SYM = 899,            /* SCHEDULE_SYM  */
    SCHEMA_NAME_SYM = 900,         /* SCHEMA_NAME_SYM  */
    SECOND_SYM = 901,              /* SECOND_SYM  */
    SECURITY_SYM = 902,            /* SECURITY_SYM  */
    SEQUENCE_SYM = 903,            /* SEQUENCE_SYM  */
    SERIALIZABLE_SYM = 904,        /* SERIALIZABLE_SYM  */
    SERIAL_SYM = 905,              /* SERIAL_SYM  */
    SESSION_SYM = 906,             /* SESSION_SYM  */
    SERVER_SYM = 907,              /* SERVER_SYM  */
    SETVAL_SYM = 908,              /* SETVAL_SYM  */
    SHARE_SYM = 909,               /* SHARE_SYM  */
    SHUTDOWN = 910,                /* SHUTDOWN  */
    SIGNED_SYM = 911,              /* SIGNED_SYM  */
    SIMPLE_SYM = 912,              /* SIMPLE_SYM  */
    SLAVE = 913,                   /* SLAVE  */
    SLAVES = 914,                  /* SLAVES  */
    SLAVE_POS_SYM = 915,           /* SLAVE_POS_SYM  */
    SLOW = 916,                    /* SLOW  */
    SNAPSHOT_SYM = 917,            /* SNAPSHOT_SYM  */
    SOCKET_SYM = 918,              /* SOCKET_SYM  */
    SOFT_SYM = 919,                /* SOFT_SYM  */
    SONAME_SYM = 920,              /* SONAME_SYM  */
    SOUNDS_SYM = 921,              /* SOUNDS_SYM  */
    SOURCE_SYM = 922,              /* SOURCE_SYM  */
    SQL_BUFFER_RESULT = 923,       /* SQL_BUFFER_RESULT  */
    SQL_CACHE_SYM = 924,           /* SQL_CACHE_SYM  */
    SQL_CALC_FOUND_ROWS = 925,     /* SQL_CALC_FOUND_ROWS  */
    SQL_NO_CACHE_SYM = 926,        /* SQL_NO_CACHE_SYM  */
    SQL_THREAD = 927,              /* SQL_THREAD  */
    STAGE_SYM = 928,               /* STAGE_SYM  */
    STARTS_SYM = 929,              /* STARTS_SYM  */
    START_SYM = 930,               /* START_SYM  */
    STATEMENT_SYM = 931,           /* STATEMENT_SYM  */
    STATUS_SYM = 932,              /* STATUS_SYM  */
    STOP_SYM = 933,                /* STOP_SYM  */
    STORAGE_SYM = 934,             /* STORAGE_SYM  */
    STORED_SYM = 935,              /* STORED_SYM  */
    STRING_SYM = 936,              /* STRING_SYM  */
    SUBCLASS_ORIGIN_SYM = 937,     /* SUBCLASS_ORIGIN_SYM  */
    SUBDATE_SYM = 938,             /* SUBDATE_SYM  */
    SUBJECT_SYM = 939,             /* SUBJECT_SYM  */
    SUBPARTITIONS_SYM = 940,       /* SUBPARTITIONS_SYM  */
    SUBPARTITION_SYM = 941,        /* SUBPARTITION_SYM  */
    SUPER_SYM = 942,               /* SUPER_SYM  */
    SUSPEND_SYM = 943,             /* SUSPEND_SYM  */
    SWAPS_SYM = 944,               /* SWAPS_SYM  */
    SWITCHES_SYM = 945,            /* SWITCHES_SYM  */
    SYSTEM = 946,                  /* SYSTEM  */
    SYSTEM_TIME_SYM = 947,         /* SYSTEM_TIME_SYM  */
    TABLES = 948,                  /* TABLES  */
    TABLESPACE = 949,              /* TABLESPACE  */
    TABLE_CHECKSUM_SYM = 950,      /* TABLE_CHECKSUM_SYM  */
    TABLE_NAME_SYM = 951,          /* TABLE_NAME_SYM  */
    TEMPORARY = 952,               /* TEMPORARY  */
    TEMPTABLE_SYM = 953,           /* TEMPTABLE_SYM  */
    TEXT_SYM = 954,                /* TEXT_SYM  */
    THAN_SYM = 955,                /* THAN_SYM  */
    TIES_SYM = 956,                /* TIES_SYM  */
    TIMESTAMP = 957,               /* TIMESTAMP  */
    TIMESTAMP_ADD = 958,           /* TIMESTAMP_ADD  */
    TIMESTAMP_DIFF = 959,          /* TIMESTAMP_DIFF  */
    TIME_SYM = 960,                /* TIME_SYM  */
    TRANSACTION_SYM = 961,         /* TRANSACTION_SYM  */
    TRANSACTIONAL_SYM = 962,       /* TRANSACTIONAL_SYM  */
    THREADS_SYM = 963,             /* THREADS_SYM  */
    TRIGGERS_SYM = 964,            /* TRIGGERS_SYM  */
    TRIM_ORACLE = 965,             /* TRIM_ORACLE  */
    TRUNCATE_SYM = 966,            /* TRUNCATE_SYM  */
    TYPES_SYM = 967,               /* TYPES_SYM  */
    TYPE_SYM = 968,                /* TYPE_SYM  */
    UDF_RETURNS_SYM = 969,         /* UDF_RETURNS_SYM  */
    UNBOUNDED_SYM = 970,           /* UNBOUNDED_SYM  */
    UNCOMMITTED_SYM = 971,         /* UNCOMMITTED_SYM  */
    UNDEFINED_SYM = 972,           /* UNDEFINED_SYM  */
    UNDOFILE_SYM = 973,            /* UNDOFILE_SYM  */
    UNDO_BUFFER_SIZE_SYM = 974,    /* UNDO_BUFFER_SIZE_SYM  */
    UNICODE_SYM = 975,             /* UNICODE_SYM  */
    UNINSTALL_SYM = 976,           /* UNINSTALL_SYM  */
    UNKNOWN_SYM = 977,             /* UNKNOWN_SYM  */
    UNTIL_SYM = 978,               /* UNTIL_SYM  */
    UPGRADE_SYM = 979,             /* UPGRADE_SYM  */
    USER_SYM = 980,                /* USER_SYM  */
    USE_FRM = 981,                 /* USE_FRM  */
    VALUE_SYM = 982,               /* VALUE_SYM  */
    VARCHAR2_MARIADB_SYM = 983,    /* VARCHAR2_MARIADB_SYM  */
    VARCHAR2_ORACLE_SYM = 984,     /* VARCHAR2_ORACLE_SYM  */
    VARIABLES = 985,               /* VARIABLES  */
    VERSIONING_SYM = 986,          /* VERSIONING_SYM  */
    VIA_SYM = 987,                 /* VIA_SYM  */
    VIEW_SYM = 988,                /* VIEW_SYM  */
    VISIBLE_SYM = 989,             /* VISIBLE_SYM  */
    VIRTUAL_SYM = 990,             /* VIRTUAL_SYM  */
    WAIT_SYM = 991,                /* WAIT_SYM  */
    WARNINGS = 992,                /* WARNINGS  */
    WEEK_SYM = 993,                /* WEEK_SYM  */
    WEIGHT_STRING_SYM = 994,       /* WEIGHT_STRING_SYM  */
    WINDOW_SYM = 995,              /* WINDOW_SYM  */
    WITHIN = 996,                  /* WITHIN  */
    WITHOUT = 997,                 /* WITHOUT  */
    WORK_SYM = 998,                /* WORK_SYM  */
    WRAPPER_SYM = 999,             /* WRAPPER_SYM  */
    WRITE_SYM = 1000,              /* WRITE_SYM  */
    X509_SYM = 1001,               /* X509_SYM  */
    XA_SYM = 1002,                 /* XA_SYM  */
    XML_SYM = 1003,                /* XML_SYM  */
    YEAR_SYM = 1004,               /* YEAR_SYM  */
    CONDITIONLESS_JOIN = 1005,     /* CONDITIONLESS_JOIN  */
    ON_SYM = 1006,                 /* ON_SYM  */
    PREC_BELOW_NOT = 1007,         /* PREC_BELOW_NOT  */
    SUBQUERY_AS_EXPR = 1008,       /* SUBQUERY_AS_EXPR  */
    PREC_BELOW_IDENTIFIER_OPT_SPECIAL_CASE = 1009, /* PREC_BELOW_IDENTIFIER_OPT_SPECIAL_CASE  */
    USER = 1010,                   /* USER  */
    PREC_BELOW_CONTRACTION_TOKEN2 = 1011, /* PREC_BELOW_CONTRACTION_TOKEN2  */
    EMPTY_FROM_CLAUSE = 1012       /* EMPTY_FROM_CLAUSE  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 195 "/home/buildbot/git/sql/sql_yacc.yy"

  int  num;
  ulong ulong_num;
  ulonglong ulonglong_number;
  longlong longlong_number;
  uint sp_instr_addr;

  /* structs */
  LEX_CSTRING lex_str;
  Lex_ident_cli_st kwd;
  Lex_ident_cli_st ident_cli;
  Lex_ident_sys_st ident_sys;
  Lex_column_list_privilege_st column_list_privilege;
  Lex_string_with_metadata_st lex_string_with_metadata;
  Lex_spblock_st spblock;
  Lex_spblock_handlers_st spblock_handlers;
  Lex_length_and_dec_st Lex_length_and_dec;
  Lex_cast_type_st Lex_cast_type;
  Lex_field_type_st Lex_field_type;
  Lex_dyncol_type_st Lex_dyncol_type;
  Lex_for_loop_st for_loop;
  Lex_for_loop_bounds_st for_loop_bounds;
  Lex_trim_st trim;
  Lex_substring_spec_st substring_spec;
  vers_history_point_t vers_history_point;
  struct
  {
    enum sub_select_type unit_type;
    bool distinct;
  } unit_operation;
  struct
  {
    SELECT_LEX *first;
    SELECT_LEX *prev_last;
  } select_list;
  SQL_I_List<ORDER> *select_order;
  Lex_select_lock select_lock;
  Lex_select_limit select_limit;
  Lex_order_limit_lock *order_limit_lock;

  /* pointers */
  Lex_ident_sys *ident_sys_ptr;
  Create_field *create_field;
  Spvar_definition *spvar_definition;
  Row_definition_list *spvar_definition_list;
  const Type_handler *type_handler;
  const class Sp_handler *sp_handler;
  CHARSET_INFO *charset;
  Condition_information_item *cond_info_item;
  DYNCALL_CREATE_DEF *dyncol_def;
  Diagnostics_information *diag_info;
  Item *item;
  Item_num *item_num;
  Item_param *item_param;
  Item_basic_constant *item_basic_constant;
  Key_part_spec *key_part;
  LEX *lex;
  sp_expr_lex *expr_lex;
  sp_assignment_lex *assignment_lex;
  class sp_lex_cursor *sp_cursor_stmt;
  LEX_CSTRING *lex_str_ptr;
  LEX_USER *lex_user;
  USER_AUTH *user_auth;
  List<Condition_information_item> *cond_info_list;
  List<DYNCALL_CREATE_DEF> *dyncol_def_list;
  List<Item> *item_list;
  List<sp_assignment_lex> *sp_assignment_lex_list;
  List<Statement_information_item> *stmt_info_list;
  List<String> *string_list;
  List<Lex_ident_sys> *ident_sys_list;
  Statement_information_item *stmt_info_item;
  String *string;
  TABLE_LIST *table_list;
  Table_ident *table;
  Qualified_column_ident *qualified_column_ident;
  char *simple_string;
  const char *const_simple_string;
  chooser_compare_func_creator boolfunc2creator;
  class Lex_grant_privilege *lex_grant;
  class Lex_grant_object_name *lex_grant_ident;
  class my_var *myvar;
  class sp_condition_value *spcondvalue;
  class sp_head *sphead;
  class sp_name *spname;
  class sp_variable *spvar;
  class With_element_head *with_element_head;
  class With_clause *with_clause;
  class Virtual_column_info *virtual_column;

  handlerton *db_type;
  st_select_lex *select_lex;
  st_select_lex_unit *select_lex_unit;
  struct p_elem_val *p_elem_value;
  class Window_frame *window_frame;
  class Window_frame_bound *window_frame_bound;
  udf_func *udf;
  st_trg_execution_order trg_execution_order;

  /* enums */
  enum enum_sp_suid_behaviour sp_suid;
  enum enum_sp_aggregate_type sp_aggregate_type;
  enum enum_view_suid view_suid;
  enum Condition_information_item::Name cond_info_item_name;
  enum enum_diag_condition_item_name diag_condition_item_name;
  enum Diagnostics_information::Which_area diag_area;
  enum enum_fk_option m_fk_option;
  enum Item_udftype udf_type;
  enum Key::Keytype key_type;
  enum Statement_information_item::Name stmt_info_item_name;
  enum enum_filetype filetype;
  enum enum_tx_isolation tx_isolation;
  enum enum_var_type var_type;
  enum enum_yes_no_unknown m_yes_no_unk;
  enum ha_choice choice;
  enum ha_key_alg key_alg;
  enum ha_rkey_function ha_rkey_mode;
  enum index_hint_type index_hint;
  enum interval_type interval, interval_time_st;
  enum row_type row_type;
  enum sp_variable::enum_mode spvar_mode;
  enum thr_lock_type lock_type;
  enum enum_mysql_timestamp_type date_time_type;
  enum Window_frame_bound::Bound_precedence_type bound_precedence_type;
  enum Window_frame::Frame_units frame_units;
  enum Window_frame::Frame_exclusion frame_exclusion;
  enum trigger_order_type trigger_action_order_type;
  DDL_options_st object_ddl_options;
  enum vers_kind_t vers_range_unit;
  enum Column_definition::enum_column_versioning vers_column_versioning;
  enum plsql_cursor_attr_t plsql_cursor_attr;
  privilege_t privilege;

#line 954 "/home/buildbot/git/mkdist/sql/yy_oracle.hh"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif




int ORAparse (THD *thd);


#endif /* !YY_ORA_HOME_BUILDBOT_GIT_MKDIST_SQL_YY_ORACLE_HH_INCLUDED  */
